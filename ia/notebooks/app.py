import warnings
import streamlit as st
warnings.filterwarnings('ignore')
from pandas import read_csv
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score
from sklearn.model_selection import KFold, cross_val_score 
from sklearn.metrics import classification_report, mean_squared_error, r2_score
# Modules d`apprentissage automatique
from sklearn.model_selection import train_test_split 
# Algorithme d`apprentissage (classifier, model)
from sklearn.linear_model import LogisticRegression,LinearRegression
from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.svm import SVC
from sklearn.preprocessing import LabelEncoder
#Création d'une fonction qui nous permet de  charger les datas:
def load_data_pima(filename):
    colnames = ['preg', 'plas', 'pres', 'skin', 'test', 'mas', 'pedi', 'age', 'class']
    return pd.read_csv(filename, names = colnames)
def load_data_housing(filename):
    colnames = ['CRIM', 'ZN', 'INDUS', 'CHAS', 'NOX', 'RM', 'AGE', 'DIS', 'RAD', 'TAX','PTRATIO', 'B', 'LSTAT', 'MEDV']
    return pd.read_csv(filename, names = colnames, delim_whitespace=True)
#Chargement des données
pima_data=load_data_pima('./Datasets/pima-indians-diabetes.data.csv')
housing_data=load_data_housing('./datasets/housing.csv')
#Fonction pour l'entrainement des données de la classification et l'évaluation
def entrainement():
    model.fit(X_train, Y_train)
    y_pred = model.predict(X_test)
    result = model.score(X_test, Y_test)
    matrix = confusion_matrix(Y_test, y_pred)

    # Changer le background et le style
    st.markdown(
        """
        <style>
        .st-ae {
            background-color: #f0f7fc;
            padding: 10px;
            border-radius: 5px;
            box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.1);
        }
        </style>
        """,
        unsafe_allow_html=True
    )

    # Affichage de la matrix de confusion
    st.markdown('<div class="st-ae">Matrix de confusion :</div>', unsafe_allow_html=True)
    st.write(matrix)

    # Calcul des métriques
    acc = accuracy_score(Y_test, y_pred)
    rec = recall_score(Y_test, y_pred)
    prec = precision_score(Y_test, y_pred)
    f1 = f1_score(Y_test, y_pred)

    # Affichage des métriques avec des couleurs personnalisées
    st.markdown('<div class="st-ae">Métriques d\'évaluation :</div>', unsafe_allow_html=True)
    st.write(f"Précision : {acc:.2f}")
    st.write(f"Rappel : {rec:.2f}")
    st.write(f"Précision positive : {prec:.2f}")
    st.write(f"Score F1 : {f1:.2f}")
    option = st.radio("Veuillez choisir une métrique pour l\'évaluation de votre algorithme :",("accuracy","recall_score","precision_score","f1_score"))
    if option == "accuracy":
        st.write("Accuracy:", acc.round(2)*100.0,"%", "(",result.std().round(2),")")
        st.info("La précision mesure la proportion de prédictions correctes parmi toutes les prédictions.")
    elif option == "recall_score":
        st.write("Recall:", rec.round(2)*100.0,"%", "(",result.std().round(2),")")
        st.info("Le rappel mesure la proportion de vrais positifs parmi tous les vrais positifs et faux négatifs.")
    elif option == "precision_score":
        st.write("Precision:", prec.round(2)*100.0,"%", "(",result.std().round(2),")")
        st.info("La précision positive mesure la proportion de vrais positifs parmi tous les vrais positifs et faux positifs.")
    elif option == "f1_score":
        st.write("F1-Score:", f1.round(2)*100.0,"%", "(",result.std().round(2),")")
        st.info("Le score F1 est une mesure équilibrée entre la précision et le rappel. Il est utile lorsque les classes sont déséquilibrées.")
#Fonction pour l'entrainement des données de la Regression et l'évaluation
def entrainement2():
    model.fit(X_train, Y_train)
    Y_pred = model.predict(X_test)
    
    # Changer le style des boutons radio
    st.markdown(
        """
        <style>
        .radio-button-wrapper .radio-button {  
            background-color: #f0f7fc;
            border: 2px solid #c8d6e5;
            border-radius: 5px;
            padding: 5px 10px;
            margin-right: 10px;
        }
        </style>
        """,
        unsafe_allow_html=True
    )

    # Radio bouton avec style personnalisé
    st.radio("Choisissez une métrique pour l'évaluation de votre algorithme :", ("Erreur quadratique moyenne", "Erreur absolue moyenne", "Coefficient de détermination (R²)"), key="metric_option")

    if st.session_state.metric_option == "Erreur quadratique moyenne":
        results = cross_val_score(model, X, Y, cv=kfold, scoring='neg_mean_squared_error')
        st.markdown("**Erreur quadratique moyenne :**")
        st.write(f"**Moyenne :** {results.mean().round(2)} | **Écart-type :** {results.std().round(2)}")

    elif st.session_state.metric_option == "Erreur absolue moyenne":
        results = cross_val_score(model, X, Y, cv=kfold, scoring='neg_mean_absolute_error')
        st.markdown("**Erreur absolue moyenne :**")
        st.write(f"**Moyenne :** {results.mean().round(2)} | **Écart-type :** {results.std().round(2)}")

    elif st.session_state.metric_option == "Coefficient de détermination (R²)":
        results = cross_val_score(model, X, Y, cv=kfold, scoring='r2')
        st.markdown("**Coefficient de détermination (R²) :**")
        st.write(f"**Moyenne :** {results.mean().round(2)} | **Écart-type :** {results.std().round(2)}")
#Sélection du type de Prédiction
prediction_type = st.sidebar.radio("Sélectionnez le type de prédiction", ("Classification", "Régression"))
if prediction_type=="Classification" :
    st.title('_Classification_')
     #Choix du dataset
    data = pima_data
     # Choix de l'algorithme
    algorithm = st.selectbox("Choisissez un algorithme", ("Régression linéaire", "Arbres de décision", "Forêts aléatoires", "SVM"))
     # Tranformer dataframe en matrice de valeurs. On enleve les labels
    array = data.values
     # On separe les inputs (X) et output(Y)
    X =  array[  :  , 0: -1] # Tout sauf la derniere colonne
    Y = array[ : ,  -1]
     # Separation de X et Y en train et test -> X_train, Y_train, X_test, Y_test
    proportion_test = 0.30
    seed = 8 # valeur qui permet la reproduction de resultat
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=proportion_test,random_state = seed)
   # Entraînement du modèle
    if algorithm == "Régression linéaire":
        model = LogisticRegression()
        entrainement()      
    elif algorithm == "Arbres de décision":
        model = DecisionTreeClassifier()
        entrainement()
    elif algorithm == "Forêts aléatoires":
        model = RandomForestClassifier()
        entrainement()
    elif algorithm == "SVM":
        model = SVC()
        entrainement()
else:
    st.title('_Régression_')
    # Choix du dataset
    data = housing_data
    # Choix de l'algorithme
    algorithm = st.selectbox("Choisissez un algorithme", ("Régression linéaire", "Arbres de décision", "Forêts aléatoires"))
    #Sépareation des X et Y
    array = data.values
    # On separe les inputs (X) et output(Y)
    X =  array[  :  , 0: -1] # Tout sauf la derniere colonne
    Y = array[ : ,  -1]
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)
    folds = 5
    seed = 10
    kfold = KFold(n_splits=5, shuffle=True, random_state = seed)
 #Entraînement de l'algorithme:
    if algorithm == "Régression linéaire":
        model = LinearRegression()
        entrainement2()
    elif algorithm == "Arbres de décision":
        model = DecisionTreeRegressor()
        entrainement2()
    elif algorithm == "Forêts aléatoires":
        model = RandomForestRegressor()
        entrainement2()
    #Prédiction en temps réel
if prediction_type == "Classification" :
    st.sidebar.write("Prédiction en temps Réel (Classification : ")
    preg = st.sidebar.slider('Preg', 0.0, 17.0, 3.8)
    plas = st.sidebar.slider('Plas', 0.0, 199.0, 120.8)
    pres = st.sidebar.slider('Pres', 0.0, 122.0, 69.1)
    skin = st.sidebar.slider('Skin', 0.0, 99.0, 20.5)
    test = st.sidebar.slider('Test', 0.0, 846.0, 79.7)
    mas = st.sidebar.slider('Mas', 0.0, 67.1, 31.9)
    pedi = st.sidebar.slider('Pedi', 0.1, 2.4, 0.4)
    age = st.sidebar.slider('Age', 21.0, 81.0, 33.0)
    input_data1 = pd.DataFrame([[preg,plas,pres,skin,test,mas,pedi,age]])
else:
    st.sidebar.write("Prédiction en temps réel (Régression)) : ")
    CRIM = st.sidebar.slider("CRIM",value=0)
    ZN = st.sidebar.slider("ZN", value=0)
    INDUS  = st.sidebar.slider("INDUS", value=0)
    CHAS = st.sidebar.slider("CHAS", value=0)
    NOX = st.sidebar.slider("NOX", value=0)
    RM = st.sidebar.slider("RM", value=0.0)
    AGE = st.sidebar.slider("AGE", value=0.0)
    DIS = st.sidebar.slider("DIS", value=0)
    RAD = st.sidebar.slider("RAD", value=0)
    TAX = st.sidebar.slider("TAX", value=0)
    PTRATIO = st.sidebar.slider("PTRATIO", value=0)
    B = st.sidebar.slider("B", value=0)
    LSTAT = st.sidebar.slider("LSTAT", value=0)
    input_data = pd.DataFrame([[ CRIM, ZN, INDUS, CHAS, NOX, RM, AGE, DIS, RAD, TAX,PTRATIO, B, LSTAT]])
#Résultat de Prédiction
if st.sidebar.button("Prédire"):
    if prediction_type == "Classification" :
        prediction = model.predict(input_data1)
        proba_patient = model.predict_proba(input_data1)
        Classes = ['Negative-Diabetes','Positive-Diabetes']
        st.subheader('Résultat')
        st.markdown(f":red`:{Classes[int(prediction)]}`")
        st.subheader('Probabilité')
        st.write(proba_patient.round(2)*100)
    else:
        prediction = model.predict(input_data)
        st.subheader("La valeur prédite est de :")
        st.write(prediction)